﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Component_1;
using System.Windows.Forms;
using System.Drawing;


namespace UnitTestProject2
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestDrawTo()
        {
            var v2 = new CheckUnitTest();

            string[] result2 = v2.getValidator("drawto 20 40", 1, 1);

            Assert.AreEqual(result2[0], "drawto", result2[1], "20", result2[2], "40");
        }

        [TestMethod]
        public void TestMoveTo()
        {
            var v = new CheckUnitTest();

            string[] result = v.getValidator("moveto 20 40", 1, 1);
            Assert.AreEqual(result[0], "moveto", result[1], "20", result[2], "40");
        }

        [TestMethod]
        public void TestingDrawFromLineWithInvalidParameter()
        {

            PictureBox display = new PictureBox();
            Cmdcompiler parse = new Cmdcompiler(new Canvas(Graphics.FromImage(new Bitmap(500, 400)), display));



            try
            {
                parse.DrawFromLine("triangle x,y", 0);
            }
            catch (Wrongparameters e)
            {
                // Assert
                StringAssert.Contains(e.Message, "Invalid Parameter given for the command");
                return;
            }

            Assert.Fail("The expected exception was not thrown.");
        }
        [TestMethod]
        public void LineSplitTest()
        {

            Compiler compile = new Compiler();
            string[] lineparse = compile.LineParser("moveto 150,150");

            string expected1 = "moveto";
            string actual1 = lineparse[0];

            string expected2 = "150,150";
            string actual2 = lineparse[1];

            Assert.AreEqual(expected1, actual1, "String wasn't not properly splited by LineParse");
            Assert.AreEqual(expected2, actual2, "String wasn't not properly splited by LineParse");
        }

        [TestMethod]
        public void TestingDrawFromLineWithInvalidCommand()
        {
            PictureBox display = new PictureBox();
            Cmdcompiler parse = new Cmdcompiler(new Canvas(Graphics.FromImage(new Bitmap(500, 400)), display));



            try
            {
                parse.DrawFromLine("test", 0);
            }
            catch (Wrongcmds e)
            {
                Console.WriteLine(e);
                // Assert
                StringAssert.Contains(e.Message, "Invalid command");
                return;
            }

            Assert.Fail("The expected exception was not thrown.");
        }

    }
}