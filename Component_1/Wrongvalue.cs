﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Component_1
{
    class Wrongvalue : Exception
    {
        public Wrongvalue() : base("Invalid command given.") { }
        public Wrongvalue(string message) : base(message) { }
        public Wrongvalue(string message, System.Exception inner) : base(message, inner) { }

        protected Wrongvalue(System.Runtime.Serialization.SerializationInfo info,
        System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
    }
}
