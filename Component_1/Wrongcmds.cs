﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Component_1
{
   public class Wrongcmds : Exception
    {
        public Wrongcmds() : base("Invalid command given.") { }
        public Wrongcmds(string message) : base(message) { }
        public Wrongcmds(string message, System.Exception inner) : base(message, inner) { }

        protected Wrongcmds(System.Runtime.Serialization.SerializationInfo info,
        System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
    }
}
