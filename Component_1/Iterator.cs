﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Component_1
{
    public class Iterator : Iteratorinterface
    {
        private Functionsupply _collection;
        private int _current = 0;
        private int _step = 1;

        // Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="collection"></param>
        public Iterator(Functionsupply collection)
        {
            this._collection = collection;
        }

        // Gets first item
        /// <summary>
        /// Gets the first element
        /// </summary>
        /// <returns></returns>
        public FunctionGenerate First()
        {
            _current = 0;
            return _collection[_current] as FunctionGenerate;
        }

        // Gets next item
        /// <summary>
        /// Gets the next element
        /// </summary>
        /// <returns></returns>
        public FunctionGenerate Next()
        {
            _current += _step;
            if (!IsDone)
                return _collection[_current] as FunctionGenerate;
            else

                return null;
        }

        // Gets or sets stepsize
        /// <summary>
        /// Steps to take
        /// </summary>
        public int Step
        {
            get { return _step; }
            set { _step = value; }
        }

        // Gets current iterator item

        public FunctionGenerate CurrentMethods
        {
            get { return _collection[_current] as FunctionGenerate; }
        }

        // Gets whether iteration is complete

        public bool IsDone
        {
            get { return _current >= _collection.Count; }

        }
    }
}
