﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Component_1
{
    class Polygon : Shape
    {
        int[] pointlist;


        /// <summary>
        /// Constructor for initializing size
        /// </summary>
        public Polygon() : base()
        {
            Console.WriteLine("rect obj created");
        }
        public Polygon(Color colour, int x, int y, int[] cordinates) : base(colour, x, y)
        {

            this.pointlist = cordinates.Skip(2).ToArray();
        }

        /// <summary>
        /// Sets the shape colour and position of the rectangle,
        /// </summary>
        /// <param name="colour">Colour as object of Color class.</param>
        /// <param name="list">Array of int for determining position and shapes,</param>
        public override void set(Color colour, params int[] list)
        {

            //list[0] is x, list[1] is y, list[2] is width, list[3] is height
            base.set(colour, list[0], list[1]);
            pointlist = list.Skip(2).ToArray();
        }

        /// <summary>
        /// Draw the shape
        /// </summary>
        /// <param name="g">Graphic object where it's drawn</param>
        /// <param name="fill">Fill or no Fill for the shape</param>
        override
        public void draw(Graphics g, bool fill)
        {
            List<Point> curvePoints = new List<Point>();
            curvePoints.Add(new Point(x, y));
            for (int i = 0; i < pointlist.Length; i += 2)
            {
                Console.WriteLine(pointlist[i] + " " + pointlist[i + 1] + "added to curvepoints");
                curvePoints.Add(new Point(pointlist[i], pointlist[i + 1]));
            }



            Pen p = new Pen(colour);
            SolidBrush b = new SolidBrush(colour);

            if (fill)
                g.FillPolygon(b, curvePoints.ToArray());
            else
                g.DrawPolygon(p, curvePoints.ToArray());

            Console.WriteLine("this is the base colour" + base.colour);
        }

    }
}
