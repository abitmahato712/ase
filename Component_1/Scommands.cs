﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Component_1
{
    class Scommands
    {
        //initialising x and y 
        private static int x = 0;
        private static int y = 0;

        //declaring color
        private static Color color;

        /// <summary>
        /// Taking the user inputs
        /// </summary>
        /// <param name="input">user input (i.e Shapes)</param>
        /// <param name="g">g holds for drawing area</param>
        /// <param name="gError">gError holds for errors</param>
        public static void parse(string input, Graphics g, Graphics gError)
        {
            //Creating object
            ShapeFactory source = new ShapeFactory();

            //Creating try catch block
            try
            {
                //Storing user input into array and splitting
                string[] arguments = input.Split(new[] { ' ' }, 2);
                string command = arguments[0];
                string parameters = "";


                if (arguments.Length > 1)
                {
                    parameters = arguments[1];
                }

                //splitting the first command and converting into lower string value
                switch (command.ToLower())
                {
                    case "":

                        break;

                    //when clear is typed
                    case "clear":
                        g.Clear(Color.Black);
                        // gError.Clear(Color.Black);
                        break;

                    //when reset is typed
                    case "reset":

                        g.Clear(Color.Black);
                        //    gError.Clear(Color.Black);

                        //initialising x and y to initial value
                        x = 0;
                        y = 0;
                        break;

                    default:
                        gError.Clear(Color.Black);
                        Font errortxtFont = new Font("Arial", 10);
                        SolidBrush errortxt = new SolidBrush(Color.Red);
                        gError.DrawString("Command does not exist for single line", errortxtFont, errortxt, 0, 0);
                        break;
                }
            }

            catch (FormatException)
            {
                //Error handling
                gError.Clear(Color.Black);
                Font errortxtFont = new Font("Arial", 10);
                SolidBrush errortxt = new SolidBrush(Color.Red);
                gError.DrawString("Command does not exist for single line", errortxtFont, errortxt, 0, 0);
                // MessageBox.Show($"Invalid argument type", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

    }
}