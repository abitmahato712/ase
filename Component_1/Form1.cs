﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Component_1
{
    public partial class Form1 : Form
    {
        private delegate void UpdateUiDelegate();

        readonly Bitmap OutputBitmap = new Bitmap(500, 500);

        readonly Bitmap temp_screen = new Bitmap(500, 500);

        readonly Canvas canvaas;

        readonly Cmdcompiler parser;



        Graphics g;
        public Form1()
        {
            InitializeComponent();
            canvaas = new Canvas(Graphics.FromImage(OutputBitmap), pictureBox1);
            parser = new Cmdcompiler(canvaas);//class for handling the drawing, pass the drawing surface to it
            canvaas.DrawCursor(Graphics.FromImage(temp_screen));
            canvaas.SetRunBtn(button1);
            canvaas.SetTextBox(textBox2);
            g = pictureBox1.CreateGraphics();
        }

        

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {

            Console.WriteLine("Inside TextCompile*");
            textBox2.Text = "";
            label4.Text = "";
            string cmd = textBox1.Text;
            //sends to parser which again sends to syntaxparser
            parser.DrawFromText(cmd);

            Console.WriteLine("Text Compiles Ends here");
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {
            
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {

            MessageBox.Show("Thanks for using my software");
            this.Close();
        }

        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {
            g.Clear(Color.White);
            textBox1.Text = "";
            textBox1.Text = "";

        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFile = new OpenFileDialog();
            openFile.Filter = " All Files(.)|*.*|Text Files(.txt)|.txt|";
            if (openFile.ShowDialog() == DialogResult.OK)
            {
                StreamReader openStream = new StreamReader(File.OpenRead(openFile.FileName));
                textBox1.Text = openStream.ReadToEnd();
                openStream.Dispose();
            }

        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFile = new SaveFileDialog();
            saveFile.Filter = "Text Files(.txt)|.txt| All Files(.)|*.*";
            if (saveFile.ShowDialog() == DialogResult.OK)
            {
                StreamWriter saveStream = new StreamWriter(File.Create(saveFile.FileName));
                saveStream.Write(textBox1.Text);
                saveStream.Dispose();
                MessageBox.Show("File Saved", " ", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("This program is like a paint applicaton", " ", MessageBoxButtons.OK);
        }

        private void c(object sender, EventArgs e)
        {

        }

        private void helpToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Use functions drawto,moveto,color to fill shape");
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void richTextBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void pictureBox1_Paint(object sender, PaintEventArgs e)
        {
            Console.WriteLine("Inside DIsplay Window Paint!!*");
            Graphics g = e.Graphics; //get graphics context of form (which is being displayed)


            //update the cursor position
            canvaas.DrawCursor(Graphics.FromImage(temp_screen));

            //if syntaxcheck is on draw nothing
            if (!parser.checksyntax)
            {
                g.DrawImageUnscaled(OutputBitmap, 0, 0);//put the off screen bitmap on the form
                g.DrawImageUnscaled(temp_screen, 0, 0);
                temp_screen.MakeTransparent();
            }

            //display error count in error count label
            if (parser.getErrorCount() != 0) label4.Text = "Error Found: " + parser.getErrorCount();
        }
    }
}