﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Component_1
{
   public class Wrongparameters : System.Exception
    {
        public Wrongparameters() : base("Invalid Parameter given for the command") { }
        public Wrongparameters(string message) : base(message) { }
        public Wrongparameters(string message, System.Exception inner) : base(message, inner) { }

        protected Wrongparameters(System.Runtime.Serialization.SerializationInfo info,
        System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
    }
}
