﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Component_1
{
   public class FunctionGenerate
    {
        /// <summary>
        /// Name for the Method
        /// </summary>
        public string methodName { get; set; }
        /// <summary>
        /// Paramteres that are used in the Methods
        /// </summary>
        public string[] parameters { get; set; }
        /// <summary>
        /// Content of the method
        /// </summary>
        public ArrayList content;

        /// <summary>
        /// Constructor to initialize the ArrayList
        /// </summary>
        public FunctionGenerate()
        {
            content = new ArrayList();
        }
        /// <summary>
        /// Content of the method is stored here.
        /// </summary>
        /// <param name="line"></param>
        public void AddContent(string line)
        {
            content.Add(line);
        }
        /// <summary>
        /// Clears all the content inside the method.
        /// </summary>
        public void Clear()
        {
            methodName = "";
            parameters = null;
            content.Clear();
        }
    }
}
