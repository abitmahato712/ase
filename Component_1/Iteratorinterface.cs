﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Component_1
{
    interface Iteratorinterface
    {
        FunctionGenerate First();

        FunctionGenerate Next();

        bool IsDone { get; }

        FunctionGenerate CurrentMethods { get; }
    }
}
